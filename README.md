
<h1 align="center">Alamatar Task Management Demo</h1>

<p align="center">
  <a href="https://alamatar-task-management.vercel.app/home"><strong>Demo Available here</strong></a>
  <br>
</p>

## SETUP
npm Install ( recommeded v.16 - node)
ng serve (to start on certain port , add flag --port PORT_NO)
ng build (to build)

(if not able to setup due to versioning issue or strict eslint , let me know on zaheet.work@gmail.com - have removed webpack bundle but was used while development( due to some technical issue on vercel deployment with versioning ), adding in package2.json for reference )

common solutions:
NODE_OPTIONS=--openssl-legacy-provider && ng build
NODE_OPTIONS=--openssl-legacy-provider && ng serve

## ⚗️ Features

- Strict mode.
- Lazy loading.
- Smart and pure components pattern.
- SCAM pattern.
- Self-contained components and encapsulated modules.
- Components types (e.g. component, page).
- Dynamic titles and content meta tags.
- TailwindCSS + Autoprefixer + PurgeCSS setup.
- Dark mode and theme configuration.
- Scalable CSS architecture in favor of TailwindCSS layers.

## 📄 Pages
```
Pages

- General
  - home
  - not-found
- Auth
  - sign-in
  - sign-in
- User
  - my-profile
- Task
  - home : ''
  - tasks/all
  - tasks/today
  - tasks/deleted
  - tasks/done
  - Add New Task (dialog)
  - Details ( Update )

  ## 🧱 Self-contained components

- footer
- header
- layout

## 📡 Services

- AuthService
- SeoService
- ThemeService
- SharedService
- TaskService

## 📛 Custom directives

- click-outside (detects when the user clicks outside an element).

## 🧪 Custom pipes

- bytes (transforms a numeric value into bytes, KB, MB, GB, etc.).



- Set a default theme (First time load)

  Go to `src\app\@core\services\theme\theme.config.ts` and change the following line of code

  from operating system preference

  ```ts
  export const DEFAULT_BASE_THEME = ThemeList.System;
  ```

  to light mode

  ```ts
  export const DEFAULT_BASE_THEME = ThemeList.Light;
  ```

  or dark mode

  ```ts
  export const DEFAULT_BASE_THEME = ThemeList.Dark;
  ```

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ROUTER_UTILS } from '@core/utils/router.utils';
import { SignInPage } from './pages/sign-in/sign-in.page';

const routes: Routes = [
  {
    path: ROUTER_UTILS.config.auth.signIn,
    component: SignInPage,
    data: {
      title: 'Focus on the business logic of your next application',
      description:
        'Testing SEO compatibility with angular from zaheet test account',
      robots: 'index, follow',
    },
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}

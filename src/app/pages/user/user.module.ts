import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MyProfilePage } from './pages/my-profile/my-profile.page';
import { UserRoutingModule } from './user-routing.module';

@NgModule({
  declarations: [MyProfilePage],
  imports: [CommonModule, UserRoutingModule],
})
export class UserModule {}

import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SearchPipe } from '@core/pipes/search/search.pipe';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { AddComponent } from './add/add.component';
import { DetailsComponent } from './details/details.component';
import { FiltersComponent } from './filters/filters.component';
import { ListComponent } from './list/list.component';
import { TaskRoutingModule } from './task-routing.module';
import { TasksPage } from './tasks.page';



@NgModule({
  declarations: [TasksPage, AddComponent, DetailsComponent, ListComponent, FiltersComponent, SearchPipe],
  imports: [
    CommonModule,
    FormsModule,
    BsDatepickerModule.forRoot(),
    TaskRoutingModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    TypeaheadModule.forRoot(),
    RouterModule.forChild([
      {
        path: '',
        component: TasksPage,
        data: {
          title: 'Dashboard',
          robots: 'noindex, nofollow',
        },
      },
    ]),
  ],
  providers: [DatePipe]
})
export class TaskModule {}

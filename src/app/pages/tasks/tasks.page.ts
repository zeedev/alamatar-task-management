import { Component } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ROUTER_UTILS } from '@core/utils/router.utils';
import * as moment from 'moment';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { SharedService } from 'src/app/services/shared/shared.service';
import { AddComponent } from './add/add.component';

@Component({
  templateUrl: './tasks.page.html',
  styleUrls: ['./tasks.page.scss'],
})
export class TasksPage {
  path = ROUTER_UTILS.config.base;
  type: any;
  bsModalRef?: BsModalRef;
  constructor(private modalService: BsModalService,private sharedService: SharedService, private route: ActivatedRoute, private router: Router) {
    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationEnd) {
        this.type = this.route.snapshot.paramMap.get('id');
        this.sharedService.pageType.next(this.type);
        if(this.type == 'today'){
          this.sharedService.taskFilters.next({searchedDate: moment(new Date())})
        } else {
          this.sharedService.taskFilters.next({searchedDate:''})
        }
        console.log("type", this.type)
      }
    });
  }

  addTask() :void {
    const initialState: ModalOptions = {
      initialState: {
        title: 'Add New Task'
      }
    };
    this.bsModalRef = this.modalService.show(AddComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
    this.bsModalRef.content.result.subscribe((result: any) => {
      console.log('results', result);
  })
  }
}

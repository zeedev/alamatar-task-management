import { Component, Input, OnInit } from '@angular/core';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { Constants } from 'src/app/global/constants';
import { SharedService } from 'src/app/services/shared/shared.service';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {
  GROUPS : any = Constants.GROUP
  groupArr: any;
  @Input()
  type : any;
  taskFilters : any = {
  }
  constructor(
    private sharedService: SharedService
  ) {
    this.groupArr = Object.keys(this.GROUPS).map(key => {
      return this.GROUPS[key];
    })
  }
  selected: any;
  selectedGroup: any = [];

  onSelect(event: TypeaheadMatch): void {
    this.selected = event.item.title;
    console.log("aa",event.item)
    this.selectedGroup.push(event.item.id)
    this.taskFilters.selectedGroup = this.selectedGroup;
    this.groupArr = this.groupArr.filter((grp:any) => {
      return grp.id !=event.item.id;
    })
    this.selected = ''
    this.updateFilterData()

  }

  removeChip(group:any){
    console.log(group)
    this.groupArr.push(group)
    console.log(this.selectedGroup)
    this.selectedGroup = this.selectedGroup.filter((id:any) => id != group.id);
    this.taskFilters.selectedGroup = this.selectedGroup;
    this.updateFilterData()
  }



  ngOnInit(): void {

  }

  parseDate(dateString: string): Date {
    if (dateString) {
        return new Date(dateString);
    } else {
      return new Date()
    }
}

  onSelectSolution(date: any = null) {
    console.log(date)
    // just emit the event
    this.updateFilterData()
  }

  updateFilterData(){
    this.sharedService.taskFilters.next(this.taskFilters); // emit the value to the shared service
  }
  changeDate(event: any) {
    console.log(event);
  }
  clearFilters(){
    this.taskFilters = {}
    this.taskFilters.searchedDate = ''
    this.onSelectSolution()
  }

}

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ROUTER_UTILS } from '@core/utils/router.utils';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { Constants } from 'src/app/global/constants';
import { SharedService } from 'src/app/services/shared/shared.service';
import { TasksService } from 'src/app/services/tasks/tasks.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {
  subscription: Subscription = new Subscription;
  GROUP : any = Constants.GROUP
  Constants: any  = Constants;
  @Input()
  type : any;
  taskList: any = [];
  doneList: any = [];
  deletedList: any = [];
  Number = Number
  objectKeys = Object.keys;
  path = ROUTER_UTILS.config;

  groupedTaskList: any = [];
  filteredList: any;
  // eslint-disable-next-line @typescript-eslint/ban-types
  filterTitle!: string;
  taskFilters: any = {
  }

  filterDate: any = '';
  newArr: any[] = [];
  constructor(private tasksService: TasksService, private sharedService: SharedService) {
    this.tasksService.tasksList.subscribe((data) => {
      this.taskList = data
      this.getTaskByType()
    })
    this.tasksService.doneList.subscribe((data) => {
      this.doneList = data
      this.getTaskByType()
    })
    this.tasksService.deletedList.subscribe((data) => {
      this.deletedList = data
      this.getTaskByType()
    })

    this.subscription = this.sharedService.taskFilters.subscribe((taskFilters: any) => {
      this.taskFilters = taskFilters;
      if(taskFilters.searchedDate!=''){
        const date = moment(taskFilters.searchedDate).format("yyyy-MM-DD")
        this.filterDate = date
      } else {
        this.filterDate = ''
      }
      this.getTaskByType()

  });

  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  setTasksByGroup(data:any, date: any = null){
      const y: any[] = []
      if(this.filterDate!='' ){
        data.filter((val2: any) => {
          if((moment(val2.delivery_date).format('yyyy-MM-DD') == moment(this.filterDate).format('yyyy-MM-DD')) == true ) {
            y.push(val2)
            return val2
          }
        })
        data = y

      } else {
        data = data
      }
      const result = data.reduce(function (r: any, a: any) {
        r[a.group] = r[a.group] || [];
        r[a.group].push(a);
        return r;
      }, Object.create(null));
      this.groupedTaskList = Object.keys(result).map(key => {
        return result[key];
      })
  }

  ngOnChanges() {
    if(['done','deleted','all']){
      this.getTaskByType()
    }
    // changes.prop contains the old and the new value...
  }


  getTaskByType(){
    if(this.type == 'done'){
      this.setTasksByGroup(this.doneList)
    } else if(this.type == 'deleted'){
      this.setTasksByGroup(this.deletedList)
    } else if(this.type == 'all'){
      this.setTasksByGroup([...this.taskList,...this.doneList,...this.deletedList])
    } else if(this.type == 'today') {
      // this.getFilterValue()
      this.setTasksByGroup([...this.taskList,...this.doneList])
    } else {
      this.setTasksByGroup([...this.taskList,...this.doneList])
    }
  }
  getFilterValue(){
    if(this.type == 'today'){
      this.filterDate = new Date();
    } else {
      this.filterDate = ''
    }
  }

  doneTask(id:number){
    this.tasksService.doneTask({id})
  }



  deleteTask(id:number){
    this.tasksService.deleteTask({id})
  }

  ngOnInit(): void {
    // eslint-disable-next-line @typescript-eslint/ban-types
    
  }

}

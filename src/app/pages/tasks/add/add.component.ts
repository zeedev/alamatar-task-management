import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { Constants } from 'src/app/global/constants';
import { TasksService } from 'src/app/services/tasks/tasks.service';
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  registerForm!: FormGroup;
  submitted = false;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  result: Subject<boolean> = new Subject<boolean>();

  title?: string;
  closeBtnName?: string;
  // City Names
  Group : any = Constants.GROUP
  Constant = Constants
  objectKeys = Object.keys;
  modalRef?: BsModalRef | null;
  constructor(
    private fb: FormBuilder,
    public bsModalRef: BsModalRef,
    public tasksService: TasksService
  ) { }

  passBack() {
    this.passEntry.emit(this.registerForm.value);
  }
  ngOnInit(): void {

    this.registerForm = this.fb.group({
      title: ['', Validators.required],
      description: ['', [Validators.required ]],
      priority: ['', [Validators.required ]],
      delivery_date: ['', [Validators.required ]],
      group: ['', [Validators.required ]],
    }
    );
  }

  addToList(formValues: any){
    console.log("here")
    this.bsModalRef.hide();
  }


  get registerFormControl() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.registerForm.value.status = Constants.STATUS['PUBLISHED'];
    if (this.registerForm.valid) {
      this.tasksService.saveTask(this.registerForm.value)
      this.result.next(this.registerForm.value);
      this.bsModalRef.hide();
      console.table(this.registerForm.value);
      console.log(this.registerForm.value);
    }
  }

}

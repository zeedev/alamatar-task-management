import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Constants } from 'src/app/global/constants';
import { TasksService } from 'src/app/services/tasks/tasks.service';
import { Location } from '@angular/common';


@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  taskList: any;
  doneList: any;
  deletedList: any;
  type: any;
  task: any;
  updatedData: any;

  Group: any = Constants.GROUP
  objectKeys = Object.keys;


  registerForm!: FormGroup;
  submitted!: boolean;

  constructor(
    private tasksService: TasksService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private _location: Location ) {
    this.tasksService.tasksList.subscribe((data) => {
      this.taskList = data
    })
    this.tasksService.doneList.subscribe((data) => {
      this.doneList = data
    })
    this.tasksService.deletedList.subscribe((data) => {
      this.deletedList = data
    })

    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationEnd) {
        this.type = this.route.snapshot.paramMap.get('task_id');
      }
    });
  }

  get registerFormControl() {
    return this.registerForm.controls;
  }

  ngOnInit(): void {
    const task = [...this.taskList, ...this.doneList, ...this.deletedList].filter(task => task.id == this.type)
    this.task = task[0]

    this.registerForm = this.fb.group({
      id: ['', Validators.required],
      title: ['', Validators.required],
      description: ['', [Validators.required]],
      priority: ['', [Validators.required]],
      delivery_date: ['', [Validators.required]],
      group: ['', [Validators.required]],
      status: [''],
    }
    );
    this.registerForm.setValue(this.task)
  }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.valid) {
      this.tasksService.saveTask(this.registerForm.value);
      this.updatedData = true;
      this._location.back();
    }
  }

}

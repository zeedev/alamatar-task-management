import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ROUTER_UTILS } from '@core/utils/router.utils';
import { DetailsComponent } from './details/details.component';
import { TasksPage } from './tasks.page';

const routes: Routes = [
  { path: ROUTER_UTILS.config.base.tasks, component: TasksPage },
  { path: ROUTER_UTILS.config.task.type, component: TasksPage },
  { path: ROUTER_UTILS.config.task.detail, component: DetailsComponent },
  //tasks/deleted
  // { path: ROUTER_UTILS.config.user.overview, component: OverviewPage },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TaskRoutingModule {}

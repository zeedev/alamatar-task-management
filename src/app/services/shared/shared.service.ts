import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  searchSubject = new Subject<string>(); // a new subject of type String, replace it with your data type
  taskFilters = new Subject<any>(); // a new subject of type String, replace it with your data type
  pageType = new Subject<any>(); // a new subject of type String, replace it with your data type
}

import { Injectable } from '@angular/core';
import { getTasks, setTasks } from '@core/utils';
import { BehaviorSubject } from 'rxjs';
import { Constants } from 'src/app/global/constants';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  tasksList = new BehaviorSubject<any>([]);
  doneList = new BehaviorSubject<any>([]);
  deletedList = new BehaviorSubject<any>([]);

  constructor() {
    const currentList = getTasks('Tasks') || []
    this.tasksList.next(currentList)

    const doneList = getTasks('Done') || []
    this.doneList.next(doneList)

    const deletedList = getTasks('Deleted') || []
    this.deletedList.next(deletedList)
  }

  saveTask(task:any){
    const currentList = getTasks('Tasks') || []
    const taskPush = currentList?.length > 0 ? [...currentList,task] : [task]
    if((task.id || task.id == 0)){
      const newArrTask = taskPush.filter((item) => item.id !== task.id)
      newArrTask.push(task)
      this.tasksList.next(newArrTask)
      setTasks('Tasks',newArrTask);
    } else {
      task.id = [...this.deletedList.value,...this.doneList.value,...this.tasksList.value].length
      this.tasksList.next(taskPush)
      setTasks('Tasks',this.tasksList.value);
    }
  }



  deleteTask(data: any) {
    console.log(data)
    let valueToBeDelete ;
    const newList = this.tasksList.value.filter((item:any) => {
      if(item.id == data.id){
        valueToBeDelete = item
        valueToBeDelete.status = Constants.STATUS['DELETED'];
      }
      return item.id != data.id
    })

    this.tasksList.next(newList)
    setTasks('Tasks',newList);

    const deletedList = getTasks('Deleted') || []
    deletedList.push(valueToBeDelete);

    this.deletedList.next(deletedList);
    setTasks('Deleted', deletedList);
    return data
  }

  doneTask(data: any) {
    console.log(data)
    let valueTobDone ;
    const newList = this.tasksList.value.filter((item:any) => {
      if(item.id == data.id){
        valueTobDone = item
        valueTobDone.status = Constants.STATUS['DONE']
      }
      return item.id != data.id
    })

    this.tasksList.next(newList)
    setTasks('Tasks',newList);

    const doneList = getTasks('Done') || []
    doneList.push(valueTobDone);

    this.doneList.next(doneList);
    setTasks('Done', doneList);
    return data
  }
}

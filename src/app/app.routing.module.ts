import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard, NoAuthGuard } from '@core/guards';
import { ROUTER_UTILS } from '@core/utils/router.utils';
import { HeaderModule } from '@shell/ui/header/header.module';
import { LayoutModule } from '@shell/ui/layout/layout.module';
import { NotFoundModule } from '@shell/ui/not-found/not-found.module';
import { NotFoundPage } from '@shell/ui/not-found/not-found.page';

const APP_ROUTES: Routes = [
  {
    path: ROUTER_UTILS.config.auth.root, //auth routings
    loadChildren: async () =>
      (await import('@pages/auth/auth.module')).AuthModule,
    canLoad: [NoAuthGuard],
  },
  {
    path: ROUTER_UTILS.config.base.home, //home base routing
    loadChildren: async () =>
      (await import('@pages/home/home.module')).HomeModule,
      canLoad: [AuthGuard]
  },
  {
    path: ROUTER_UTILS.config.base.tasks, //task routings
    loadChildren: async () =>
      (await import('@pages/tasks/tasks.module')).TaskModule,
      canLoad: [AuthGuard],
  },
  {
    path: ROUTER_UTILS.config.task.root,
    loadChildren: async () =>
      (await import('@pages/tasks/tasks.module')).TaskModule,
    canLoad: [AuthGuard],
  },
  {
    path: ROUTER_UTILS.config.user.root,
    loadChildren: async () =>
      (await import('@pages/user/user.module')).UserModule,
    canLoad: [AuthGuard],
  },
  {
    path: '**',
    loadChildren: async () =>
      (await import('@shell/ui/not-found/not-found.module')).NotFoundModule,
    component: NotFoundPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(APP_ROUTES),
    HeaderModule,
    LayoutModule,
    NotFoundModule,
  ],
  exports: [
    RouterModule,
    HeaderModule,
    LayoutModule,
    NotFoundModule,
  ],
})
export class WebShellModule {}

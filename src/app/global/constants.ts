export class Constants {
  public static GROUP = {
    0: { id: 0, title: "group 0" },
    1: { id: 1, title: "group 1" },
    2: { id: 2, title: "group 2" },
  };

  public static STATUS = {
    PUBLISHED: 0,
    DONE: 1,
    DELETED: 2
  };

  public static STATUS_ID = {
    0: 'PUBLISHED',
    1: 'DONE',
    2: 'DELETED'
  };

}

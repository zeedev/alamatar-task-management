export const ROUTER_UTILS = {
  config: {
    base: {
      home: '',
      root: 'home',
      tasks: 'tasks',
      list: 'list',
    },
    deleted: {
      root: 'deleted',
    },
    done: {
      root: 'done',
    },
    week: {
      root: 'week',
    },
    auth: {
      root: 'auth',
      signIn: 'sign-in',
    },
    task: {
      root: 'task',
      type: ':id',
      detail: 'details/:task_id',
    },
    groups: {
      root: 'groups',
      details: ':id',
      tasks: ':id/list',
      taskDetails: ':id/list/:taskId',
    },
    user: {
      root: 'users',
      profile: ':username',
    },
    errorResponse: {
      notFound: '404',
    },
  },
};

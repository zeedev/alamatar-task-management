import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SearchTaskComponent } from '@shell/ft/components/search-task/search-task.component';
import { HeaderComponent } from './header.component';

@NgModule({
  declarations: [HeaderComponent, SearchTaskComponent],
  imports: [CommonModule, RouterModule],
  exports: [HeaderComponent],
})
export class HeaderModule {}

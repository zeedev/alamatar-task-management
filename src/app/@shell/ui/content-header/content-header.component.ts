import { Component } from '@angular/core';
import { SharedService } from 'src/app/services/shared/shared.service';

@Component({
  selector: 'app-content-header',
  templateUrl: './content-header.component.html',
  styleUrls: ['./content-header.component.scss']
})
export class ContentHeaderComponent {
  type: any;

  constructor(private sharedService: SharedService) {
    this.sharedService.pageType.subscribe((data) => {
      this.type = data
    })
  }

}

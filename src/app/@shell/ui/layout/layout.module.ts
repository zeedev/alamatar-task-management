import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ContentHeaderComponent } from '../content-header/content-header.component';
import { HeaderModule } from '../header/header.module';
import { LayoutComponent } from './layout.component';

@NgModule({
  declarations: [LayoutComponent, ContentHeaderComponent],
  imports: [CommonModule, HeaderModule],
  exports: [LayoutComponent],
})
export class LayoutModule {}

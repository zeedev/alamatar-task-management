import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard, NoAuthGuard } from '@core/guards';
import { ROUTER_UTILS } from '@core/utils/router.utils';
import { NotFoundModule } from '@shell/ui/not-found/not-found.module';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { HeaderModule } from '../ui/header/header.module';
import { LayoutModule } from '../ui/layout/layout.module';
import { NotFoundPage } from '../ui/not-found/not-found.page';


const APP_ROUTES: Routes = [
  {
    path: ROUTER_UTILS.config.auth.root, //auth 
    loadChildren: async () =>
      (await import('@pages/auth/auth.module')).AuthModule,
    canLoad: [NoAuthGuard],
  },
  {
    path: ROUTER_UTILS.config.base.home, //home base 
    loadChildren: async () =>
      (await import('@pages/home/home.module')).HomeModule,
      canLoad: [AuthGuard]
  },
  {
    path: ROUTER_UTILS.config.base.tasks, //base task 
    loadChildren: async () =>
      (await import('@pages/tasks/tasks.module')).TaskModule,
      canLoad: [AuthGuard],
  },
  {
    path: ROUTER_UTILS.config.task.root, //task details
    loadChildren: async () =>
      (await import('@pages/tasks/tasks.module')).TaskModule,
    canLoad: [AuthGuard],
  },
  {
    path: ROUTER_UTILS.config.user.root, //user profile
    loadChildren: async () =>
      (await import('@pages/user/user.module')).UserModule,
    canLoad: [AuthGuard],
  },
  {
    path: '**', //unknown route
    loadChildren: async () =>
      (await import('@shell/ui/not-found/not-found.module')).NotFoundModule,
    component: NotFoundPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(APP_ROUTES),
    HeaderModule,
    LayoutModule,
    NotFoundModule,
    TypeaheadModule,
    FormsModule
  ],
  exports: [
    RouterModule,
    HeaderModule,
    LayoutModule,
    NotFoundModule,
  ],
})
export class WebShellModule {}
